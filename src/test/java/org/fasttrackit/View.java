package org.fasttrackit;

import java.util.Scanner;

public class View {
    public static final String CHECK_ACCOUNT_BALANCE = "2";
    public static final String VIEW_ACCOUNT_DETAILES = "1";
    public static final String CREATE_NEW_ACCOUNT = "3";
    public static final String EXIT = "6";

     static void showWelcomingScreen() {
        System.out.println("Welcome to the new FastTrack application");
        System.out.println();
        System.out.println("What operation would like to do?");
        System.out.println("1.Register new account");
        System.out.println("2.Login with existing account");
        System.out.print("Your selection:");
    }

    static void showContactSupport() {
        System.out.println("You have reached max login attempts");
        System.out.println("Please contact support more details");
        System.out.println("Contact us via email:support@fasttrackbank.org or via phone 0800 800 800");
    }

     static void showNavigationMenu() {
        System.out.println("....................");
        System.out.println("1.View account details");
        System.out.println("2.Check account balance");
        System.out.println("3.Create new account");
        System.out.println("4.Exchange rates");
        System.out.println("5.Transfer between accounts");
         System.out.println("6. Exit");
        System.out.println("What would like to do?");
    }
    static void showAccountDetailes(Account account) {
        System.out.println("IBAN:" + account.getIban());
        System.out.println("Account Currency:" + account.getCurrency());
        System.out.println("Balance:" + account.getBalance());
    }
    static void showSupportedCurrency() {
        System.out.println("1.Ron");
        System.out.println("2.EUR");
        System.out.println("3.USD");
        System.out.println("4.GBP");
        System.out.println("Select currency for the new account");
    }

    static void askUserForRegistrationDetails(Scanner keyboard) {
        System.out.print("Please enter user");
        String user = keyboard.nextLine();
        System.out.print("Please enter password");
        String password = keyboard.nextLine();

        System.out.print("Please enter name");
        String name = keyboard.nextLine();
        System.out.println("Please enter surname");
        String surname = keyboard.nextLine();

        System.out.println("Pleased enter email: ");
        String email = keyboard.nextLine();
        System.out.print("Please enter phone number");
        String phoneNr = keyboard.nextLine();
    }
}