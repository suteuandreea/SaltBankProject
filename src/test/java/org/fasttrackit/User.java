package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;

//user -> (user / password / Name / Surname / email / Pho
public class User {

    private final Credentials credentials;
    private final Account defaultAccount;
    private final List<Account> accounts;
    private final String name;
    private final String surname;
    private String email;
    private String phone;

    public User(String loginId, String password, String name, String surname) {
        this.credentials = new Credentials (password,loginId);
        this.defaultAccount = new Account(  "RO69BTRLRONCRT67875444",  "RON");
        this.name = name;
        this.surname = surname;
        this.email = "";
        this.phone = "";
        this.accounts = new ArrayList<>();
    }
    public void registerAccount(Account newAccount) {
     this.accounts.add(newAccount);
    }

    public boolean isAuthenticated(Credentials credentials) {
        return this.credentials.equals(credentials);
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public Account getDefaultAccount() {
        return defaultAccount;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setEmail(String email) {
        this.email = email;
    }


}
