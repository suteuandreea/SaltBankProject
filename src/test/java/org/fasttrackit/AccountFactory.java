package org.fasttrackit;

public class AccountFactory {
    static Account createAccount(String userOption) {
        Account account = null;
        switch (userOption) {
            case "1": {
                account = new Account( "RO69BTRLRONCRT67878244", "RON");
                break;
            }
            case "2":{
                account = new Account( "RO69BTRLEURCRT67878244", "EUR");
                break;
            }
            case "3": {
                account = new Account( "RO69BTUSDCRT67878244", "USD");
                break;
            }
            case "4": {
                account = new Account( "RO69BTRLGBPCRT67878244", "GBP");
                break;
            }
            default:
                System.out.println("Sorry unknown option");
        }
        return account;
    }
}
